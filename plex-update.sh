#!/bin/bash

## Root check
if [[ $UID -ne 0 ]]; then
    echo "Must be run as root"; 
    exit 1;
fi

	      ## Functions ##
############################################

function logger()
{
    echo "[$(date -u)]: ${1}" 
}

function variable_check() 
{

    if [[ -z $pms_unit_file ]]; then
	logger "pms_unit_file not set, exiting..."
	exit 1;
    fi

    if [[ -z $plex_pass ]]; then
	logger "plex_pass config not set, exiting..."
	exit 1;
    fi

    if [[ -z $plex_token ]]; then
	logger "plex_token not set, attempting to obtain..."
	get_plex_token;
    fi

    if [[ -z $download_dir ]]; then
	logger "download_dir not set, exiting..."
	exit 1;
    fi

    if [[ -z $log_file ]]; then
	logger "log_file not set, using default /var/log/plex-update.log"
	log_file="/var/log/plex-update.log"
    else
	[[ ! -f ${log_file} ]] && touch "${log_file}"
    fi

    ## Setting the proper update channel
    [[ ${plex_pass} -eq 1 ]] && channel=8 || channel=16

}

## Obtain server token by parsing Plex's 
## preferences.xml file

function get_plex_token()
{

    plex_token=$(sed -e 's/ /\n/g' "${plex_preferences}"  | grep PlexOnlineToken | sed -e 's/PlexOnlineToken=//' -e 's/\"//g')

}

## Check if update is available, uses same source
## as Plex to determine if one is available.

function check_for_update()
{
    curl -s -o "${download_dir}/update.xml" -J -L ${update_url}
    
    if [[ $(sed -e 's/ /\n/g' "${download_dir}/update.xml" | grep size=\"1\") ]]; then
	update_available=1;
    else 
	update_available=0;
    fi
    if [[ -f "${download_dir}/update.xml" ]]; then
        rm -f "${download_dir}/update.xml"
    fi
}

## Function to get name of the file that will be 
## downloaded. Some cases where update shows available
## but the old version still downloads.

function getUriFilename() {
    header="$(curl -sI "$1" | tr -d '\r')"
    filename="$(logger "$header" | grep -o -E 'filename=.*$')"
    if [[ -n "$filename" ]]; then
	logger "${filename#filename=}"
	return
    fi
			
    filename="$(logger "$header" | grep -o -E 'Location:.*$')"
    if [[ -n "$filename" ]]; then
	basename "${filename#Location\:}"
	return
    fi

    return 1
}

## Function that checks version of the download against
## the installed version

function compare_version_num(){

    installed=$(rpm -q plexmediaserver | sed -e 's/plexmediaserver-//' -e 's/\.x86_64//')

    new=$(logger "${1}" | sed -e 's/plexmediaserver-//' -e 's/\.x86_64//')

    if [[ ${new} == ${installed} ]]; then
	logger "Vesion to be downloaded is already installed"
	return 5;
    else
	logger "New version is available: ${new}"
	return 1;
    fi
}

## As the name implies, downloads the update and 
## installs it. Also starts and stops plex.

function update_plex() {

	logger "Downloading RPM file..."
	curl -o "${download_dir}/pms.rpm" -J -L "${download_url}"
	
	logger "Stopping Plex Service"
	systemctl stop "${pms_unit_file}"

	logger "Updating Plex Media Server"
	rpm -Uhv "${download_dir}/pms.rpm"
	
	logger "Restarting Plex Media Server"
	systemctl start "${pms_unit_file}"

}

## Cleanup after ourselves

function clean_up()
{
    if [[ -f "${download_dir}/pms.rpm" ]]; then
        logger "Deleting plex RPM file"
	rm -f "${download_dir}/pms.rpm"
    fi

    logger "Deleting update.xml file"
    rm -f "${download_dir}/update.xml"

    logger "Finished Cleanup"
}

## Main function that contains all the logics

function main() {

	      ## Static Variables ##
    ############################################ 

    ## Path to Plex Preferences

    plex_preferences='/var/lib/plexmediaserver/Library/Application Support/Plex Media Server/Preferences.xml'

    ## installed version number

    ver_num=$(rpm -q plexmediaserver | sed -e 's/plexmediaserver-//' -e 's/\.x86_64//')

    ## URL where plex RPM is downloaded from

    download_url="https://plex.tv/downloads/latest/1?channel=${channel}&build=linux-ubuntu-x86_64&distro=redhat&X-Plex-Token=${plex_token}"

    ## URL used to check if new version is available

    update_url="https://plex.tv/updater/products/1/check.xml?build=linux-ubuntu-x86_64&channel=${channel}&distribution=redhat&version=${ver_num}&X-Plex-Token=${plex_token}"

    ############################################
    echo "############################################"
    logger "Running update script"
    echo "############################################"

    ## Sanity check
    variable_check

    ## See if update is avalable
    check_for_update
    
    if [[ ${update_available} -eq 1 ]]; then

	## Make sure new versions being downloaded

	rpm_filename=$(getUriFilename ${url})
	
	compare_version_num ${rpm_filename}
	
	if [[ $? -eq 1 ]]; then
	    update_plex;
	    if [[ ${delete_rpm} -eq 1 ]]; then
		logger "Beginning Cleanup"
		clean_up;
		exit 0;
	    fi
	else
	    logger "Update available but download is not"
	    exit 0;
	fi
    else
	logger "No update available"
	exit 0;
    fi
}

## Source plex-update.conf file

if [[ -f "./plex-update.conf" ]]; then
    source ./plex-update.conf
else
    logger "plex-update.conf not found"
    exit 1;
fi

## RUNNIT!
main >> "${log_file}" 
