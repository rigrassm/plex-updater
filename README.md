## Plex Media Server Update Script

### Requirements

- Plex Media Server
- RPM based distribution(Tested on Fedora 24)
- Systemd

### Usage

Clone the project onto your server then open the
plex-update.conf file. Go through and edit the
variables to suit your needs and save the file.

Once you've set your configuration settings,
make the plex-update.sh script executable.

After the above is complete, simply run the
plex-update.sh script as root.

    sudo ./plex-update.sh

All output fron the script is logged to the
file specified in your plex-update.conf

### Automating with a Systemd Timer

- Coming soon
